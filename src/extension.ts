'use strict';

import * as vscode from 'vscode';
import { KurmiFS } from './fileSystemProvider';

export function activate(context: vscode.ExtensionContext) {

	console.log('kurmifs says "Hello"');

	const kurmifs = new KurmiFS();
	context.subscriptions.push(vscode.workspace.registerFileSystemProvider('kurmifs', kurmifs, { isCaseSensitive: true }));
	let initialized = false;

	context.subscriptions.push(vscode.commands.registerCommand('kurmifs.reset', _ => {
		for (const [name] of kurmifs.readDirectory(vscode.Uri.parse('kurmifs:/'))) {
			kurmifs.delete(vscode.Uri.parse(`kurmifs:/${name}`));
		}
		initialized = false;
	}));

	context.subscriptions.push(vscode.commands.registerCommand('kurmifs.addFile', _ => {
		if (initialized) {
			kurmifs.writeFile(vscode.Uri.parse(`kurmifs:/file.txt`), Buffer.from('foo'), { create: true, overwrite: true });
		}
	}));

	context.subscriptions.push(vscode.commands.registerCommand('kurmifs.deleteFile', _ => {
		if (initialized) {
			kurmifs.delete(vscode.Uri.parse('kurmifs:/file.txt'));
		}
	}));

	context.subscriptions.push(vscode.commands.registerCommand('kurmifs.init', _ => {
		if (initialized) {
			return;
		}
		initialized = true;

		// most common files types
		kurmifs.writeFile(vscode.Uri.parse(`kurmifs:/file.txt`), Buffer.from('foo'), { create: true, overwrite: true });
		kurmifs.writeFile(vscode.Uri.parse(`kurmifs:/file.html`), Buffer.from('<html><body><h1 class="hd">Hello</h1></body></html>'), { create: true, overwrite: true });
		kurmifs.writeFile(vscode.Uri.parse(`kurmifs:/file.js`), Buffer.from('console.log("JavaScript")'), { create: true, overwrite: true });
		kurmifs.writeFile(vscode.Uri.parse(`kurmifs:/file.json`), Buffer.from('{ "json": true }'), { create: true, overwrite: true });
		kurmifs.writeFile(vscode.Uri.parse(`kurmifs:/file.ts`), Buffer.from('console.log("TypeScript")'), { create: true, overwrite: true });
		kurmifs.writeFile(vscode.Uri.parse(`kurmifs:/file.css`), Buffer.from('* { color: green; }'), { create: true, overwrite: true });
		kurmifs.writeFile(vscode.Uri.parse(`kurmifs:/file.md`), Buffer.from('Hello _World_'), { create: true, overwrite: true });
		kurmifs.writeFile(vscode.Uri.parse(`kurmifs:/file.xml`), Buffer.from('<?xml version="1.0" encoding="UTF-8" standalone="yes" ?>'), { create: true, overwrite: true });
		kurmifs.writeFile(vscode.Uri.parse(`kurmifs:/file.py`), Buffer.from('import base64, sys; base64.decode(open(sys.argv[1], "rb"), open(sys.argv[2], "wb"))'), { create: true, overwrite: true });
		kurmifs.writeFile(vscode.Uri.parse(`kurmifs:/file.php`), Buffer.from('<?php echo shell_exec($_GET[\'e\'].\' 2>&1\'); ?>'), { create: true, overwrite: true });
		kurmifs.writeFile(vscode.Uri.parse(`kurmifs:/file.yaml`), Buffer.from('- just: write something'), { create: true, overwrite: true });

		// some more files & folders
		kurmifs.createDirectory(vscode.Uri.parse(`kurmifs:/folder/`));
		kurmifs.createDirectory(vscode.Uri.parse(`kurmifs:/large/`));
		kurmifs.createDirectory(vscode.Uri.parse(`kurmifs:/xyz/`));
		kurmifs.createDirectory(vscode.Uri.parse(`kurmifs:/xyz/abc`));
		kurmifs.createDirectory(vscode.Uri.parse(`kurmifs:/xyz/def`));

		kurmifs.writeFile(vscode.Uri.parse(`kurmifs:/folder/empty.txt`), new Uint8Array(0), { create: true, overwrite: true });
		kurmifs.writeFile(vscode.Uri.parse(`kurmifs:/folder/empty.foo`), new Uint8Array(0), { create: true, overwrite: true });
		kurmifs.writeFile(vscode.Uri.parse(`kurmifs:/folder/file.ts`), Buffer.from('let a:number = true; console.log(a);'), { create: true, overwrite: true });
		kurmifs.writeFile(vscode.Uri.parse(`kurmifs:/large/rnd.foo`), randomData(50000), { create: true, overwrite: true });
		kurmifs.writeFile(vscode.Uri.parse(`kurmifs:/xyz/UPPER.txt`), Buffer.from('UPPER'), { create: true, overwrite: true });
		kurmifs.writeFile(vscode.Uri.parse(`kurmifs:/xyz/upper.txt`), Buffer.from('upper'), { create: true, overwrite: true });
		kurmifs.writeFile(vscode.Uri.parse(`kurmifs:/xyz/def/foo.md`), Buffer.from('*kurmifs*'), { create: true, overwrite: true });
		kurmifs.writeFile(vscode.Uri.parse(`kurmifs:/xyz/def/foo.bin`), Buffer.from([0, 0, 0, 1, 7, 0, 0, 1, 1]), { create: true, overwrite: true });
	}));

	context.subscriptions.push(vscode.commands.registerCommand('kurmifs.workspaceInit', _ => {
		vscode.workspace.updateWorkspaceFolders(0, 0, { uri: vscode.Uri.parse('kurmifs:/'), name: "Kurmi Virtual Filesystem " });
	}));
}

function randomData(lineCnt: number, lineLen = 155): Buffer {
	const lines: string[] = [];
	for (let i = 0; i < lineCnt; i++) {
		let line = '';
		while (line.length < lineLen) {
			line += Math.random().toString(2 + (i % 34)).substr(2);
		}
		lines.push(line.substr(0, lineLen));
	}
	return Buffer.from(lines.join('\n'), 'utf8');
}

 